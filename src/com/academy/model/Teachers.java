package com.academy.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class Teachers implements Serializable {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private int tId;

	private String teacher_Name;
	

	public Teachers(){
		
		
	}
	
	@OneToMany(cascade=CascadeType.ALL,targetEntity = Subjects.class,fetch = FetchType.LAZY)
	@JoinTable(name="Teachers_Subjects", joinColumns={@JoinColumn(name="subId")})
	//@JoinColumn(name="subId")
	List<Subjects> teacher = new ArrayList<>();

	@OneToMany(cascade=CascadeType.ALL,targetEntity = Classes.class,fetch = FetchType.LAZY)
	@JoinTable(name="Teachers_Classes", joinColumns={@JoinColumn(name="cId")})
//	@JoinColumn(name="cId")
	List<Classes> teachr = new ArrayList<>();

	public int gettId() {
		return tId;
	}

	public void settId(int tId) {
		this.tId = tId;
	}

	public String getTeacherName() {
		return teacher_Name;
	}

	public void setTeacherName(String teacherName) {
		this.teacher_Name = teacherName;
	}

	public List<Subjects> getLstSub() {
		return teacher;
	}

	public void setLstSub(List<Subjects> lstSub) {
		this.teacher = lstSub;
	}

	public List<Classes> getLstCls() {
		return teachr;
	}

	public void setLstCls(List<Classes> lstCls) {
		this.teachr = lstCls;
	}

	public Teachers(int tId, String teacherName, List<Subjects> lstSub, List<Classes> lstCls) {
		super();
		this.tId = tId;
		this.teacher_Name = teacherName;
		this.teacher = lstSub;
		this.teachr = lstCls;
	}
	
	public Teachers( String teacherName, List<Subjects> lstSub, List<Classes> lstCls) {
		super();
		this.teacher_Name = teacherName;
		this.teacher = lstSub;
		this.teachr = lstCls;
	}

}
