package com.academy.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Subjects {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private int subId;

	private String sub_Name;

//	@ManyToMany(mappedBy="lstSub")
  //  private List<Teachers> teachers =new ArrayList<>();

	public Subjects(){
		
		
	}
	
	public int getSubId() {
		return subId;
	}

	public void setSubId(int subId) {
		this.subId = subId;
	}

	public String getSubName() {
		return sub_Name;
	}

	public void setSubName(String subName) {
		this.sub_Name = subName;
	}



	public Subjects(int subId, String subName) {
		super();
		this.subId = subId;
		this.sub_Name = subName;
	}

	@Override
	public String toString() {
		return "Subjects [subId=" + subId + ", subName=" + sub_Name + " ]";
	};
	
	

	
}
