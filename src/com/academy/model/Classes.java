package com.academy.model;

import java.io.Serializable;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Classes implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int cId;
	@Column(unique = true)
	private int class_Std;
	
	public Classes(){
		
	}
	
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public int getClassStd() {
		return class_Std;
	}
	public void setClassStd(int classStd) {
		this.class_Std = classStd;
	}
	public Classes(int cId, int classStd) {
		super();
		this.cId = cId;
		this.class_Std = classStd;
	}
	public Classes(int classStd) {
		super();
		this.class_Std = classStd;
	}
	
	
	}
