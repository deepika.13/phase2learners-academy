package com.academy.model;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table
public class Students {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int sId;
	private String stud_Name ;
	@Column(unique=true)
	private int stud_Badge ;
	@OneToOne(cascade=CascadeType.ALL,targetEntity = Classes.class,fetch = FetchType.LAZY)
	@JoinColumn(name = "cId")
	private Classes stu_Class;
	
	public Students(){
		
		
	}
	
	public Students(String studName, int studBadge, Classes stuClass) {
		super();
		this.stud_Name = studName;
		this.stud_Badge = studBadge;
		this.stu_Class = stuClass;
	}
	public Students(int sId, String studName, int studBadge, Classes stuClass) {
		super();
		this.sId = sId;
		this.stud_Name = studName;
		this.stud_Badge = studBadge;
		this.stu_Class = stuClass;
	}
	public void setClsId(Classes clsId) {
		this.stu_Class = clsId;
	}
	public int getsId() {
		return sId;
	}
	public void setsId(int sId) {
		this.sId = sId;
	}

	public String getStudName() {
		return stud_Name;
	}
	public void setStudName(String studName) {
		this.stud_Name = studName;
	}
	public int getStudBadge() {
		return stud_Badge;
	}
	public void setStudBadge(int studBadge) {
		this.stud_Badge = studBadge;
	}
/*	public int getClas() {
		return clas.getcId();
	}
	public void setClas(Classes clas) {
		this.clas.setcId(clas.getcId());
	}*/
	@Override
	public String toString() {
		return "Student [sId=" + sId + ", studName=" + stud_Name + ", studBadge=" + stud_Badge + ", clas=" + stu_Class + "]";
	}

	
	

}
