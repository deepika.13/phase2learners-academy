package com.academy.controller;

import java.lang.reflect.Type;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.academy.model.Classes;
import com.academy.model.Students;
import com.academy.model.Subjects;
import com.academy.model.Teachers;
import com.academy.service.Service;
import com.academy.service.impl.ClassesServiceImpl;
import com.academy.service.impl.StudentsServiceImpl;
import com.academy.service.impl.SubjectsServiceImpl;
import com.academy.service.impl.TeachersServiceImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;


@Path("/")
public class AcademyController {


	public String welcome(){

		return  "Welcome to Learners Academy";

	}

	ClassesServiceImpl serviceClas = new ClassesServiceImpl();
	StudentsServiceImpl serviceStud = new StudentsServiceImpl();	
	SubjectsServiceImpl serviceSub = new SubjectsServiceImpl();
	TeachersServiceImpl serviceTeach = new TeachersServiceImpl();
	Gson gson = new Gson();
	
	@Path("Classes")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Classes> createClas(String clas) throws SQLIntegrityConstraintViolationException
	{
		//String clas ="[{'classStd':1},{'classStd':2}]";
		System.out.println("Entered ClassModule "+clas);
		//List<ClassModule> lstCm = new ArrayList<>();
		Type classModuleListType = new TypeToken<ArrayList<Classes>>(){}.getType();
		System.out.println("Type ClassModule "+classModuleListType.getTypeName());
		List<Classes> lstCm = gson.fromJson(clas, classModuleListType); 
		System.out.println(lstCm.isEmpty());
		//System.out.println(clas.get(0).getClassStd());
			return serviceClas.createClas(lstCm);
			
	}
	
	@Path("Classes")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Classes> getAllClasses()
	{
		
		return serviceClas.getAllClasses();
	
	}



	@Path("Students")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	
	public List<Students> addStu(String student)
	{
		System.out.println("Entered ClassModule "+student);

		Type StudentsListType = new TypeToken<ArrayList<Students>>(){}.getType();
		List<Students> lstStud = gson.fromJson(student, StudentsListType); 
		
		for(Students stu : lstStud)
		{
			System.out.println(stu.getStudBadge());
			System.out.println(stu.getsId());
			System.out.println(stu.getStudName());

		//System.out.println("class id : "+stu.getClsId().getcId());
		}
		return serviceStud.addStudent(lstStud);

	}

	@Path("Students")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Students> getAllStudents()
	{
		
		return serviceStud.getAllStudents();
	
	}
	
	@Path("Subjects")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Subjects> addSub(String subject)
	{
		System.out.println("Entered SubjectModule "+subject);

		Type SubjectsListType = new TypeToken<ArrayList<Subjects>>(){}.getType();
		List<Subjects> lstSub = gson.fromJson(subject, SubjectsListType); 
		
		for(Subjects sub : lstSub)
		{
			System.out.println(sub.getSubId());
			System.out.println(sub.getSubName());


		//System.out.println("class id : "+stu.getClsId().getcId());
		}

		return serviceSub.addSubject(lstSub);

	}
	
	@Path("Subjects")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Subjects> getAllSubjects()
	{
		
		return serviceSub.getAllSubjects();
	
	}

	@Path("Teachers")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Teachers> addTeacher(String teach)
	{

		System.out.println("Entered TeacherModule "+teach);

		Type SubjectsListType = new TypeToken<ArrayList<Teachers>>(){}.getType();
		List<Teachers> lstTeach = gson.fromJson(teach, SubjectsListType); 
		
		for(Teachers teacher : lstTeach)
		{
			System.out.println(teacher.getTeacherName());
			System.out.println(teacher.gettId());
		//	System.out.println(teacher.getSubId());



		//System.out.println("class id : "+stu.getClsId().getcId());
		}
		return serviceTeach.addTeacher(lstTeach);
	}

	@Path("Teachers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Teachers> getAllTeachers()
	{
		
		return serviceTeach.getAllTeachers();
	
	}

}
