package com.academy.dao;

import java.util.List;

import com.academy.model.Students;



public interface StudentsDao {


	public List<Students> addStudent(List<Students> student);
	public List<Students> getAllStudents();
	public void removeStudent(int id);
	public Students updateStudent(Students student);
}
