package com.academy.dao;

import java.util.List;

import com.academy.model.Subjects;


public interface SubjectsDao {

	public List<Subjects> addSubject(List<Subjects> subject);
	public List<Subjects> getAllSubjects();
	public void removeSubject(int id);
	public Subjects updateStudent(Subjects subject);

}
