package com.academy.dao.impl;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.academy.dao.ClassesDao;
import com.academy.dao.StudentsDao;
import com.academy.dao.SubjectsDao;
import com.academy.dao.TeachersDao;
import com.academy.model.Classes;
import com.academy.model.Students;
import com.academy.model.Subjects;
import com.academy.model.Teachers;

public class ClassesDaoImpl implements ClassesDao , StudentsDao , SubjectsDao , TeachersDao {

	Configuration configuration = new Configuration().configure();
	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory = configuration.buildSessionFactory(builder.build());


	@Override
	/*	public Classess createClas(Classess clas) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(clas);
		transaction.commit();
		session.close();
		return clas; //List<ClassModule>
	}*/
	public List<Classes> createClas(List<Classes> clas) throws SQLIntegrityConstraintViolationException {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		for(Classes objCm :clas)
		{
			System.out.println("loop : " +objCm.getClassStd());
			session.save(objCm);
		}
		transaction.commit();
		session.close();
		return clas;

	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Classes> getAllClasses() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Classes> clasList =  new ArrayList<Classes>();
	//	System.out.println("Query :"+session.createQuery("select cId, class_Std from com.academy.model.Classes").list().size());
		clasList =  session.createQuery("from com.academy.model.Classes").list();
		transaction.commit();
		session.close();
		return clasList;
	}

	@Override
	public void removeClas(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Classes updateClas(Classes clas) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Teachers> addTeacher(List<Teachers> teacher) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		for(Teachers teac:teacher){
			session.save(teac);
		}
		transaction.commit();
		session.close();
		return teacher;
	}

	@Override
	public List<Teachers> getAllTeachers() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Teachers> clasList =  new ArrayList<Teachers>();
		System.out.println("Query :"+session.createQuery("from com.academy.model.Teachers").list().size());
		clasList =  session.createQuery("from com.academy.model.Teachers").list();
		transaction.commit();
		session.close();
		return clasList;
	}

	@Override
	public void removeTeacher(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Teachers updateTeacher(Teachers teacher) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Subjects> addSubject(List<Subjects> subject) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		for(Subjects sub:subject){
			session.save(sub);
		}
		transaction.commit();
		session.close();
		return subject;
	}

	@Override
	public List<Subjects> getAllSubjects() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Subjects> clasList =  new ArrayList<Subjects>();
	//	System.out.println("Query :"+session.createQuery("select cId, class_Std from com.academy.model.Subjects").list().size());
		clasList =  session.createQuery("from com.academy.model.Subjects").list();
		transaction.commit();
		session.close();
		return clasList;
	}

	@Override
	public void removeSubject(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Subjects updateStudent(Subjects subject) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Students> addStudent(List<Students> student) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		for(Students stu : student){
			/*stu.getStudBadge();
			stu.getStudName();
			stu.getClas();*/
			System.out.println("Indide for loop Students of  DAo");
			System.out.println(student);
			//	System.out.println("cid value DAo"+stu.getClas().getcId());
			//System.out.println("Entered Daoimplem "+stu.getStudBadge()+"name"+stu.getStudName()+"stu.getClas()"+stu.getClas());
			//System.out.println("stu.getClas().getClassStd()"+stu.getClas());

			session.save(stu);

		}

		transaction.commit();
		session.close();
		return student;

	}

	@Override
	public List<Students> getAllStudents() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Students> clasList =  new ArrayList<Students>();
	//	System.out.println("Query :"+session.createQuery("select cId, class_Std from com.academy.model.Students").list().size());
		clasList =  session.createQuery("from com.academy.model.Students").list();
		transaction.commit();
		session.close();
		return clasList;
	}

	@Override
	public void removeStudent(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Students updateStudent(Students student) {
		// TODO Auto-generated method stub
		return null;
	}

}
