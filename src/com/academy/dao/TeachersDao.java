package com.academy.dao;

import java.util.List;

import com.academy.model.Teachers;

public interface TeachersDao {
	
	
	public List<Teachers> addTeacher(List<Teachers> teacher);
	public List<Teachers> getAllTeachers();
	public void removeTeacher(int id);
	public Teachers updateTeacher(Teachers teacher);

}
