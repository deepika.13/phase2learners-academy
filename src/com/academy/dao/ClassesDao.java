package com.academy.dao;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import com.academy.model.Classes;

public interface ClassesDao {

	//public Classess createClas(Classess clas);
	public List<Classes> createClas(List<Classes> clas) throws SQLIntegrityConstraintViolationException;
	public List<Classes> getAllClasses();
	public void removeClas(int id);
	public Classes updateClas(Classes clas);
	
}
