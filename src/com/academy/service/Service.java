package com.academy.service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import com.academy.model.Classes;
import com.academy.model.Students;
import com.academy.model.Subjects;
import com.academy.model.Teachers;

public class Service {
	
	public interface ClassesService {

		public List<Classes> createClas(List<Classes> clas) throws SQLIntegrityConstraintViolationException;
		public List<Classes> getAllClasses();
		public void removeClas(int id);
		public Classes updateClas(Classes clas);
		
	}

	public interface StudentsService {

		public List<Students> addStudent(List<Students> student);
		public List<Students> getAllStudents();
		public void removeStudent(int id);
		public Students updateStudent(Students student);
		
		
	}

	public interface SubjectsService {

		public List<Subjects> addSubject(List<Subjects> subject);
		public List<Subjects> getAllSubjects();
		public void removeSubject(int id);
		public Subjects updateStudent(Subjects subject);
	}

	public interface TeachersService {
		
		
		public List<Teachers> addTeacher(List<Teachers> teacher);
		public List<Teachers> getAllTeachers();
		public void removeTeacher(int id);
		public Teachers updateTeacher(Teachers teacher);

	}
	
}
